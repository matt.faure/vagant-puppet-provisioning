class mymodule (
  Stdlib::Httpsurl $repos_url = 'https://gitlab.adullact.net/Comptoir/Comptoir.git',
) {
  file { 'My debug file':
    ensure  => present,
    path    => '/tmp/my-debug.txt',
    content => $repos_url,
  }
}